package com.android_mvp_base_2018.mvpbase.retrofit;


import com.android_mvp_base_2018.library.CustomException;

/**
 * Created by sukumar on 04-02-2018
 */
public interface IApiResponseListener<T> {

    void onSuccessfulApi(long taskId, T response);

    void onFailureApi(long taskId, CustomException e);

}
