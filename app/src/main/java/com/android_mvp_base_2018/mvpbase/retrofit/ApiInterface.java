package com.android_mvp_base_2018.mvpbase.retrofit;


import com.android_mvp_base_2018.mvpbase.model.dto.response.ListResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * Created by sukumar on 04-02-2018
 */
public interface ApiInterface {

    //@GET("android/jsonandroid/")
    //Call<ListResponse> loginApi();

    @GET("android/jsonandroid/")
    Observable<ListResponse> loginApi();






   /* @Headers(Constants.Header.HEADER_CONTENT_TYPE)
    @POST("auth_user")
    Call<LoginResponse> nativeLogin(@Body LoginRequest loginRequest);


    @Headers(Constants.Header.HEADER_CONTENT_TYPE)
    @GET("mykenlists")
    Call<MyKenListResponse> callKenListApi(@Header("Authorization") String token);


    @Headers(Constants.Header.HEADER_CONTENT_TYPE)
    @GET("sharedkenlists")
    Call<SharedResponse> callSharedKenList(@Header("Authorization") String token);


    @Headers(Constants.Header.HEADER_CONTENT_TYPE)
    @POST
    Call<MyNewKenResponse> createNewKenList(@Header("Authorization") String token, @Url String url);


    @Headers(Constants.Header.HEADER_CONTENT_TYPE)
    @POST
    Call<BaseResponse> callPlayListDeleteApi(@Header("Authorization") String token, @Url String url);


    @Headers(Constants.Header.HEADER_CONTENT_TYPE)
    @POST
    Call<BaseResponse> callSharewithEmail(@Header("Authorization") String token, @Url String url);


    @Headers(Constants.Header.HEADER_CONTENT_TYPE)
    @POST("search")
    Call<SearchResponse> searchRequestAPi(@Body SearchRequest searchRequest, @Header("Authorization") String token);


    @Headers(Constants.Header.HEADER_CONTENT_TYPE)
    @GET("notifications")
    Call<NotificationResponse> requestNotificationApi(@Header("Authorization") String token);


    @Headers(Constants.Header.HEADER_CONTENT_TYPE)
    @GET
    Call<KenListDetailsResponse> callKenDetailsOfKenList(@Header("Authorization") String token, @Url String url);


    @Headers(Constants.Header.HEADER_CONTENT_TYPE)
    @POST("discussions")
    Call<CreateDiscussionResponse> callCreateDiscussionAPi(@Body DiscussionRequestWithContentId requestWithContentId, @Header("Authorization") String token);


    @Headers(Constants.Header.HEADER_CONTENT_TYPE)
    @POST
    Call<CreateDiscussionResponse> callCreateDiscussionAPiWithCourseId(@Header("Authorization") String token, @Url String url);


    @Headers(Constants.Header.HEADER_CONTENT_TYPE)
    @GET
    Call<ViewNotesResponse> callViewNoteApi(@Header("Authorization") String token, @Url String url);


    @Headers(Constants.Header.HEADER_CONTENT_TYPE)
    @POST
    Call<BaseResponse> callContentLikeApi(@Header("Authorization") String token, @Url String url);


    @Headers(Constants.Header.HEADER_CONTENT_TYPE)
    @GET
    Call<DiscussionListResponse> callDiscussionList(@Header("Authorization") String token, @Url String url);


    @Headers(Constants.Header.HEADER_CONTENT_TYPE)
    @GET
    Call<DiscussionListResponse> callDiscussionListWithCourseId(@Header("Authorization") String token, @Url String url);


    @Headers(Constants.Header.HEADER_CONTENT_TYPE)
    @POST
    Call<DiscussionReplyReponse> callDiscussionReplyApi(@Body DiscussionReplyRequest replyRequest, @Header("Authorization") String token, @Url String url);


    @Headers(Constants.Header.HEADER_CONTENT_TYPE)
    @GET
    Call<PhraseCloudResponse> callPhraseCloudApi(@Header("Authorization") String token, @Url String url);


    @Headers(Constants.Header.HEADER_CONTENT_TYPE)
    @GET
    Call<PhraseCloudTimeResponse> callPhraseCloudTimeApi(@Header("Authorization") String token, @Url String url);


    @Headers(Constants.Header.HEADER_CONTENT_TYPE)
    @GET
    Call<PhraseCloudResponse> callPhraseGenerateApi(@Header("Authorization") String token, @Url String url);


    @Headers(Constants.Header.HEADER_CONTENT_TYPE)
    @POST("setbookmark")
    Call<BaseResponse> callAddNoteApi(@Body AddNoteRequest addNoteRequest, @Header("Authorization") String token);


    @Headers(Constants.Header.HEADER_CONTENT_TYPE)
    @GET
    Call<TableContentResponse> callTableContentApi(@Header("Authorization") String token, @Url String url);


    @Headers(Constants.Header.HEADER_CONTENT_TYPE)
    @GET
    Call<BaseResponse> callTableRequestApi(@Header("Authorization") String token, @Url String url);


    @Headers(Constants.Header.HEADER_CONTENT_TYPE)
    @POST
    Call<MyKenDetLectureList> callAddKenIntoKenListApi(@Header("Authorization") String token, @Url String url);

    @Headers(Constants.Header.HEADER_CONTENT_TYPE)
    @POST
    Call<BaseResponse> callContentDeleteApi(@Header("Authorization") String token, @Url String url);


    @Headers(Constants.Header.HEADER_CONTENT_TYPE)
    @POST
    Call<BaseResponse> callReArrangePositinApi(@Header("Authorization") String token, @Url String url);

    @Headers(Constants.Header.HEADER_CONTENT_TYPE)
    @GET("demo_video")
    Call<DemoResponse> demoVideoApi(@Header("Authorization") String token);


    @Headers(Constants.Header.HEADER_CONTENT_TYPE)
    @GET("courses")
    Call<MyCourseResponse> callMyCourseListApi(@Header("Authorization") String token);


    @Headers(Constants.Header.HEADER_CONTENT_TYPE)
    @GET
    Call<MyCourseDetailResponse> callCourseDetailsApi(@Header("Authorization") String token, @Url String url);

    @Headers(Constants.Header.HEADER_CONTENT_TYPE)
    @GET
    Call<CourseSessionDetailResponse> callCourseSessionDetailsApi(@Header("Authorization") String token, @Url String url);

    @Headers(Constants.Header.HEADER_CONTENT_TYPE)
    @GET
    Call<SessionQuizListResponse> callSessionQuizListApi(@Header("Authorization") String token, @Url String url);


    @Headers(Constants.Header.HEADER_CONTENT_TYPE)
    @GET
    Call<QuizDetailsResponse> callSessionQuizDetailsApi(@Header("Authorization") String token, @Url String url);

    @Headers(Constants.Header.HEADER_CONTENT_TYPE)
    @GET("curriculums")
    Call<MyCurriculumResponse> callListOfCurriculumAPI(@Header("Authorization") String token);


    @Headers(Constants.Header.HEADER_CONTENT_TYPE)
    @GET
    Call<ResponseBody> callCurriculumDetailsApi(@Header("Authorization") String token, @Url String url);// stupid api integration.Because of client response.

    @Headers(Constants.Header.HEADER_CONTENT_TYPE)
    @GET
    Call<ResponseBody> callQuizQuestionListApi(@Header("Authorization") String token, @Url String url);


    @Headers(Constants.Header.HEADER_CONTENT_TYPE)
    @POST
    Call<ResponseBody> callWxamQuizSubmitApi(@Header("Authorization") String token, @Url String url);*/

}

