package com.android_mvp_base_2018.mvpbase.retrofit;

import com.android_mvp_base_2018.mvpbase.common.Constants;
import com.android_mvp_base_2018.mvpbase.util.SharedPref;
import com.github.aurae.retrofit2.LoganSquareConverterFactory;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;


/**
 * Created by sukumar on 04-02-2018
 */
public class ApiClient {
    private static Retrofit retrofit = null;
    private static OkHttpClient.Builder httpClient;


    public static Retrofit getRetrofit() {
        if (retrofit == null) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            // set your desired log level
            logging.setLevel(HttpLoggingInterceptor.Level.BASIC);

            if (httpClient == null)
                httpClient = new OkHttpClient.Builder();

            httpClient.readTimeout(60, TimeUnit.SECONDS);

            httpClient.connectTimeout(60, TimeUnit.SECONDS);

            //use below line for uploading large files
            //httpClient.writeTimeout(1, TimeUnit.HOURS);

            // add logging as last interceptor
            httpClient.addInterceptor(logging);

            retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.URL.BASE_URL)
                    .client(httpClient.build())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(LoganSquareConverterFactory.create())
                    .build();
        }

        //header fun
        if (SharedPref.getInstance().getToken() != null) {
            OkHttpClient okHttpClient = getHeaderClient(SharedPref.getInstance().getToken());
            retrofit = retrofit.newBuilder().client(okHttpClient).build();
        }

        return retrofit;
    }


    //add token inside header
    private static OkHttpClient getHeaderClient(final String accessToken) {

        if (httpClient == null)
            httpClient = new OkHttpClient.Builder();

        httpClient.addInterceptor(chain -> {
            Request original = chain.request();
            Request request = original.newBuilder()
                    .header(Constants.ApiHeaderKeys.HEADER_AUTH_TOKEN, accessToken)
                    .method(original.method(), original.body())
                    .build();
            return chain.proceed(request);
        });
        return httpClient.build();
    }

}
