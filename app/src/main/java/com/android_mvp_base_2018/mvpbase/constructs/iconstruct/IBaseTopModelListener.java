package com.android_mvp_base_2018.mvpbase.constructs.iconstruct;

/**
 * Created by Sukumar on 25-02-2018
 */

public interface IBaseTopModelListener {

    void unAuthLogout();

    boolean isNetworkAvailable();

    void showNetworkErrorMessage();

    void showForceUpdateDialog();

    void showProgressBar();

    void dismissProgressBar();


}
