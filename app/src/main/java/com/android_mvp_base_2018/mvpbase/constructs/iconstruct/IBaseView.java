package com.android_mvp_base_2018.mvpbase.constructs.iconstruct;

import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.view.View;

import com.android_mvp_base_2018.library.CustomException;
import com.android_mvp_base_2018.mvpbase.common.IPermissionResponseListener;


/**
 * Created by sukumar on 04-02-2018
 */
public interface IBaseView {

    void showMessage(String message);

    void showMessage(int resId);

    void showMessage(CustomException e);

    void showProgressbar();

    void dismissProgressbar();

    FragmentActivity getActivity();

    void showSnackBar(String message);

    void showSnackBar(@NonNull View view, String message);

    void showNetworkErrorMessage();

    void showPermissionDenied();

    boolean isNetworkEnabled();

    void showUnAuthWindow();

    void showForceUpdateDialog();

    void showRetryOption();

    void requestSinglePermission(String permission, IPermissionResponseListener permissionResponseListener);

    void requestMultiplePermission(String[] permission, String[] mustPermissionValues, IPermissionResponseListener permissionResponseListener);
}
