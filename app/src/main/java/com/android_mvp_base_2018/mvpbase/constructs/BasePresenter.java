package com.android_mvp_base_2018.mvpbase.constructs;

import android.content.Intent;

import com.android_mvp_base_2018.mvpbase.constructs.iconstruct.IBasePresenter;
import com.android_mvp_base_2018.mvpbase.constructs.iconstruct.IBaseView;

import io.reactivex.Observer;
import io.reactivex.observers.DisposableObserver;


/**
 * Created by sukumar on 04-02-2018
 */

public abstract class BasePresenter<T extends IBaseView> implements IBasePresenter {

    protected String TAG = getClass().getSimpleName();

    protected T iBaseView;

    BasePresenter(T iBaseView) {
        this.iBaseView = iBaseView;

    }

    @Override
    public void onStartPresenter() {

    }

    @Override
    public void onStopPresenter() {

    }

    @Override
    public void onPausePresenter() {

    }

    @Override
    public void onResumePresenter() {

    }

    @Override
    public void onDestroyPresenter() {

    }

    @Override
    public void onActivityResultPresenter(int requestCode, int resultCode, Intent data) {

    }

    @Override
    public void unAuthLogout() {
        iBaseView.showUnAuthWindow();
    }

    @Override
    public boolean isNetworkAvailable() {
        return iBaseView.isNetworkEnabled();
    }

    @Override
    public void showNetworkErrorMessage() {
        iBaseView.showNetworkErrorMessage();
    }

    @Override
    public void showForceUpdateDialog() {
        iBaseView.showForceUpdateDialog();
    }

    @Override
    public void showProgressBar() {
        iBaseView.showProgressbar();
    }

    @Override
    public void dismissProgressBar() {
        iBaseView.dismissProgressbar();
    }


}
