package com.android_mvp_base_2018.mvpbase.constructs.iconstruct;

import android.content.Intent;
import android.os.Bundle;


/**
 * Created by sukumar on 04-02-2018
 */
public interface IBasePresenter extends IBaseTopModelListener {

    void onCreatePresenter(Bundle bundle);

    void onStartPresenter();

    void onStopPresenter();

    void onPausePresenter();

    void onResumePresenter();

    void onDestroyPresenter();

    void onActivityResultPresenter(int requestCode, int resultCode, Intent data);

}
