package com.android_mvp_base_2018.mvpbase.constructs;

import android.os.Bundle;

import com.android_mvp_base_2018.library.CustomException;
import com.android_mvp_base_2018.library.Log;
import com.android_mvp_base_2018.mvpbase.adapters.ListAdapter;
import com.android_mvp_base_2018.mvpbase.common.Constants;
import com.android_mvp_base_2018.mvpbase.constructs.iconstruct.IHomeConstruct;
import com.android_mvp_base_2018.mvpbase.model.dto.response.ListData;
import com.android_mvp_base_2018.mvpbase.model.dto.response.ListResponse;
import com.android_mvp_base_2018.mvpbase.retrofit.IApiResponseListener;
import com.android_mvp_base_2018.mvpbase.webservice.ListModel;

import java.util.List;


/**
 * Created by Sukumar on 04-02-2018
 */

public class HomePresenter extends BasePresenter<IHomeConstruct.IHomeView> implements IHomeConstruct.IHomePresenter {

    private ListAdapter mListAdapter;

    public HomePresenter(IHomeConstruct.IHomeView iBaseView) {
        super(iBaseView);
    }

    @Override
    public void onCreatePresenter(Bundle bundle) {
        callLoginApi();
    }

    @Override
    public void callLoginApi() {
        ListModel listModel = new ListModel(this, listResponseIApiResponseListener);
        listModel.loginApi(Constants.APiRequestCodes.LIST_API);
    }

    private IApiResponseListener<ListResponse> listResponseIApiResponseListener = new IApiResponseListener<ListResponse>() {
        @Override
        public void onSuccessfulApi(long taskId, ListResponse response) {
            Log.d(TAG, "Response Called");
            if (response != null && taskId == Constants.APiRequestCodes.LIST_API) {
                if (response.getAndroidOSVersionList() != null && response.getAndroidOSVersionList().size() > 0) {
                    setAdapter(response.getAndroidOSVersionList());
                } else {
                    //show list is empty
                }
            }
        }

        @Override
        public void onFailureApi(long taskId, CustomException e) {

        }
    };

    private void setAdapter(List<ListData> androidOSVersionList) {

        if (androidOSVersionList == null)
            return;

        if (mListAdapter == null) {
            mListAdapter = new ListAdapter(androidOSVersionList);
            iBaseView.setAdapter(mListAdapter);
        } else
            mListAdapter.resetItems(androidOSVersionList);

    }

    //implement this method. If you want retry option
    @Override
    public void showNetworkErrorMessage() {
        iBaseView.showNetworkErrorMessage();
        iBaseView.showRetryOption();
    }


}
