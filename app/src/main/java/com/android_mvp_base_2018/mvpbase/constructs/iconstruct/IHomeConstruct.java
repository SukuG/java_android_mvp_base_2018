package com.android_mvp_base_2018.mvpbase.constructs.iconstruct;

import com.android_mvp_base_2018.mvpbase.adapters.ListAdapter;

/**
 * Created by Sukumar on 24-02-2018
 */

public interface IHomeConstruct {

    interface IHomeView extends IBaseView {
        void setAdapter(ListAdapter mListAdapter);
    }

    interface IHomePresenter extends IBasePresenter {

        void callLoginApi();
    }
}
