package com.android_mvp_base_2018.mvpbase.webservice;


import com.android_mvp_base_2018.library.CustomException;
import com.android_mvp_base_2018.mvpbase.constructs.iconstruct.IBaseTopModelListener;
import com.android_mvp_base_2018.mvpbase.model.dto.response.ListResponse;
import com.android_mvp_base_2018.mvpbase.retrofit.ApiClient;
import com.android_mvp_base_2018.mvpbase.retrofit.ApiInterface;
import com.android_mvp_base_2018.mvpbase.retrofit.IApiResponseListener;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by sukumar on 08-11-2017
 */

public class ListModel extends BaseModel<ListResponse> {

    private long mCurrentTaskId = -1;
    private IApiResponseListener<ListResponse> iResponseListener;

    public ListModel(IBaseTopModelListener iBaseTopModelListener, IApiResponseListener<ListResponse> iResponseListener) {
        super(iBaseTopModelListener);
        this.iResponseListener = iResponseListener;
    }

    public void loginApi(long taskId) {
        this.mCurrentTaskId = taskId;
        enQueueTaskObservable(mCurrentTaskId,
                ApiClient.getRetrofit()
                        .create(ApiInterface.class)
                        .loginApi()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread()), true);
        //Don't delete
        //enQueueTaskObservable(mCurrentTaskId, ApiClient.getRetrofit().create(ApiInterface.class).loginApi());
    }


    @Override
    public void onSuccessfulApi(long taskId, ListResponse response) {
        iResponseListener.onSuccessfulApi(taskId, response);
    }

    @Override
    public void onFailureApi(long taskId, CustomException e) {
        iResponseListener.onFailureApi(taskId, e);
    }
}
