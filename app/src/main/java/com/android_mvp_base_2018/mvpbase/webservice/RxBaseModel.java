package com.android_mvp_base_2018.mvpbase.webservice;


import com.android_mvp_base_2018.library.CustomException;
import com.android_mvp_base_2018.library.ExceptionTracker;
import com.android_mvp_base_2018.library.Log;
import com.android_mvp_base_2018.mvpbase.common.Constants;
import com.android_mvp_base_2018.mvpbase.constructs.iconstruct.IBaseTopModelListener;
import com.android_mvp_base_2018.mvpbase.model.dto.response.BaseResponse;
import com.android_mvp_base_2018.mvpbase.retrofit.ApiClient;
import com.android_mvp_base_2018.mvpbase.retrofit.IApiResponseListener;
import com.android_mvp_base_2018.mvpbase.util.CodeSnippet;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;


/**
 * Created by sukumar on 04-02-2018
 */

abstract class RxBaseModel<T extends BaseResponse> implements IApiResponseListener<T> {
    private static final String TAG = "BaseModel";
    private long mCurrentTaskId = -1;

    private IBaseTopModelListener iBaseTopModelListener;

    RxBaseModel(IBaseTopModelListener iBaseTopModelListener) {
        this.iBaseTopModelListener = iBaseTopModelListener;
    }

    void enQueueTask(long taskId, Call<T> tCall) {
        this.mCurrentTaskId = taskId;
        if (iBaseTopModelListener.isNetworkAvailable()) {
            iBaseTopModelListener.showProgressBar();
            tCall.enqueue(baseModelCallBackListener);
        } else
            iBaseTopModelListener.showNetworkErrorMessage();
    }

    private Callback<T> baseModelCallBackListener = new Callback<T>() {
        @Override
        public void onResponse(Call<T> call, Response<T> response) {

            if (response.isSuccessful() && response.body() != null) {
                //now i set 200 for success. if you want like between 200 <300. then do custom here
                if (response.code() == Constants.InternalHttpCode.SUCCESS_CODE)
                    onSuccessfulApi(mCurrentTaskId, response.body());
                else {
                    CustomException customException = new CustomException(response.code(), response.body());
                    onFailureApi(mCurrentTaskId, customException);
                }
            } else if (response.code() == Constants.InternalHttpCode.UN_AUTH_CODE)
                iBaseTopModelListener.unAuthLogout();
            else if (response.code() == Constants.InternalHttpCode.FORCE_UPDATE)
                iBaseTopModelListener.showForceUpdateDialog();
            else {
                try {
                    Converter<ResponseBody, T> converter = ApiClient.getRetrofit().responseBodyConverter(BaseResponse.class, new Annotation[0]);
                    Log.e(TAG, "Response Code Try:" + response.code() + "Response msg:" + response.errorBody());
                    BaseResponse customException = converter.convert(response.errorBody());
                    Log.e(TAG, "customException: " + new CodeSnippet(null).getJsonStringFromObject(customException));

                    onFailureApi(mCurrentTaskId, new CustomException(response.code(), customException.getError()));

                } catch (IOException e) {
                    ExceptionTracker.track("");
                    e.printStackTrace();
                    onFailureApi(mCurrentTaskId, new CustomException(Constants.InternalHttpCode.INTERNAL_ERROR, "Error from server"));
                    Log.e(TAG, "Response Code Catch:" + response.code() + "Response msg:" + "Error from response");
                }
            }

            //dismiss progressbar
            iBaseTopModelListener.dismissProgressBar();
        }

        @Override
        public void onFailure(Call<T> call, Throwable t) {
            //dismiss progressbar
            iBaseTopModelListener.dismissProgressBar();
            Log.e(TAG + "Localize Message", "" + t.getLocalizedMessage());
            onFailureApi(mCurrentTaskId, new CustomException(Constants.InternalHttpCode.API_ON_FAILURE, "Api Response not called"));
        }
    };
}
