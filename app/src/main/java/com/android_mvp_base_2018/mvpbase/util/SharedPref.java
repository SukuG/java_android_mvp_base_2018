package com.android_mvp_base_2018.mvpbase.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.android_mvp_base_2018.R;
import com.android_mvp_base_2018.mvpbase.common.MvpBaseApplication;


/**
 * Created by sukumar on 04-02-2018
 */
public class SharedPref {

    // Single ton objects...
    private static SharedPreferences preference = null;
    private static SharedPref sharedPref = null;
    private String sharedPrefName = "MVPBase";

    //Single ton method for this class...
    public static SharedPref getInstance() {
        if (sharedPref != null) {
            return sharedPref;
        } else {
            sharedPref = new SharedPref();
            return sharedPref;
        }
    }

    public SharedPreferences getPreferenceInstance(Context context) {
        if (preference != null) {
            return preference;
        } else {
            //TODO: Shared Preference name has to be set....
            preference = context.getSharedPreferences(sharedPrefName, Context.MODE_PRIVATE);
            return preference;
        }
    }


    public void clearAllPreferences(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(sharedPrefName, Context.MODE_PRIVATE);
        Editor editor = preferences.edit();
        editor.clear().apply();
        editor.apply();

    }

    //set String value
    public void setSharedValue(String key, String value) {
        Editor editor = preference.edit();
        editor.putString(key, value);
        editor.apply();
    }
    //set String int value
    public void setSharedValue(String key, int value) {
        Editor editor = preference.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    //set String boolean value
    public void setSharedValue(String key, Boolean value) {
        Editor editor = preference.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }


    public String getStringValue(Context context, String key) {
        return getPreferenceInstance(context).getString(key, null);//default value
    }

    public int getIntValue(Context context, String key) {
        return getPreferenceInstance(context).getInt(key, -1);//default value
    }


    public Boolean getBooleanValue(Context context, String key) {
        return getPreferenceInstance(context).getBoolean(key, false);//default value
    }


    //status
    public int getUserId() {
        return getPreferenceInstance(MvpBaseApplication.getInstance()).getInt(MvpBaseApplication.getInstance().getString(R.string.login_user_id), -1);
    }

    public String getToken() {
        return getPreferenceInstance(MvpBaseApplication.getInstance()).getString(MvpBaseApplication.getInstance().getString(R.string.login_token), null);
    }

    public boolean loginStatus() {
        return getPreferenceInstance(MvpBaseApplication.getInstance()).getBoolean(MvpBaseApplication.getInstance().getString(R.string.is_login), false);
    }


}
