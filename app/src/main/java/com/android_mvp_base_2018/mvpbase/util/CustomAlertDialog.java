package com.android_mvp_base_2018.mvpbase.util;

import android.app.AlertDialog;
import android.content.Context;

import com.android_mvp_base_2018.library.Log;
import com.android_mvp_base_2018.mvpbase.adapters.listener.IAlertDialogListener;


/**
 * Created by sukumar on 04-02-2018
 */

public class CustomAlertDialog {
    protected String TAG = getClass().getSimpleName();
    private static CustomAlertDialog mCustomAlertDialog;
    private static AlertDialog.Builder mBuilder;
    private IAlertDialogListener iAlertDialogListener;

    private CustomAlertDialog(Context mContext, IAlertDialogListener iAlertDialogListener) {
        this.iAlertDialogListener = iAlertDialogListener;
        mBuilder = new AlertDialog.Builder(mContext);
    }

    public static CustomAlertDialog getInstance(Context mContext, IAlertDialogListener iAlertDialogListener) {
        mCustomAlertDialog = new CustomAlertDialog(mContext, iAlertDialogListener);
        return mCustomAlertDialog;
    }

    public void showMessageWithTitleTwoButton(String title, String message, boolean isCancelable, String positiveString, String negativeString) {
        mBuilder.setTitle(title);
        mBuilder.setMessage(message);
        mBuilder.setCancelable(isCancelable);
        mBuilder.setPositiveButton(positiveString, (dialog, which) -> {
            iAlertDialogListener.onClickYesOrNo(true);
            dialog.dismiss();
        });
        mBuilder.setNegativeButton(negativeString, (dialog, which) -> {
            iAlertDialogListener.onClickYesOrNo(false);
            dialog.dismiss();
        });
        showDialog();
    }


    public void showMessageWithOutTitleTwoButton(String message, boolean isCancelable, String positiveString, String negativeString) {
        mBuilder.setMessage(message);
        mBuilder.setCancelable(isCancelable);
        mBuilder.setPositiveButton(positiveString, (dialog, which) -> {
            iAlertDialogListener.onClickYesOrNo(true);
            dialog.dismiss();
        });
        mBuilder.setNegativeButton(negativeString, (dialog, which) -> {
            iAlertDialogListener.onClickYesOrNo(false);
            dialog.dismiss();
        });
        showDialog();
    }

    private void showDialog() {
        if (mBuilder != null) {
            AlertDialog alert = mBuilder.create();
            alert.show();
        } else
            Log.d(TAG, "mBuilder is null");
    }
}
