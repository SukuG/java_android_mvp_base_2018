package com.android_mvp_base_2018.mvpbase.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.android_mvp_base_2018.R;
import com.android_mvp_base_2018.mvpbase.adapters.viewholder.ListViewHolder;
import com.android_mvp_base_2018.mvpbase.model.dto.response.ListData;

import java.util.List;

/**
 * Created by Sukumar on 25-02-2018
 */

public class ListAdapter extends BaseRecyclerAdapter<ListData, ListViewHolder> {

    public ListAdapter(List<ListData> data) {
        super(data);
    }

    @Override
    public ListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ListViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.inflate_list, parent, false));
    }
}
