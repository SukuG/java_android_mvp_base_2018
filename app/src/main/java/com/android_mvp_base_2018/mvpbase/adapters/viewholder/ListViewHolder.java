package com.android_mvp_base_2018.mvpbase.adapters.viewholder;

import android.view.View;
import android.widget.TextView;

import com.android_mvp_base_2018.R;
import com.android_mvp_base_2018.mvpbase.model.dto.response.ListData;

import butterknife.BindView;

/**
 * Created by Sukumar on 25-02-2018
 */

public class ListViewHolder extends BaseViewHolder<ListData> {

    @BindView(R.id.version)
     TextView mTextViewVersion;

    @BindView(R.id.versionName)
     TextView mTextViewVersionName;

    @BindView(R.id.apiLevel)
     TextView mTextViewApiLevel;


    public ListViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    void populateData() {
        mTextViewVersion.setText(data.getVersion());
        mTextViewVersionName.setText(data.getVersionName());
        mTextViewApiLevel.setText(data.getApiLevel());
    }
}
