package com.android_mvp_base_2018.mvpbase.adapters.listener;


/**
 * Created by sukumar on 04-02-2018
 */
public interface IAlertDialogListener {

    void onClickYesOrNo(boolean onClickYesOrNoStatus);
}
