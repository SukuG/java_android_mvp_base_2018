package com.android_mvp_base_2018.mvpbase.adapters.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;

/**
 * Created by sukumar on 04-02-2018
 */

public abstract class BaseViewHolder<T> extends RecyclerView.ViewHolder {

    public T data;
    String TAG = getClass().getSimpleName();

    BaseViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void setData(T data) {
        this.data = data;
        populateData();
    }

    abstract void populateData();
}
