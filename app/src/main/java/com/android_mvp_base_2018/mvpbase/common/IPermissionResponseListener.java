package com.android_mvp_base_2018.mvpbase.common;

public interface IPermissionResponseListener {

    void onClickSinglePermissionStatus(boolean isGranted);

    void onClickMultiplePermissionStatus(boolean isGranted);
}
