package com.android_mvp_base_2018.mvpbase.common;


import android.os.Build;

import com.android_mvp_base_2018.BuildConfig;

/**
 * Created by sukumar on 04-02-2018
 */

public interface Constants {


    interface BundleKey {
        String BROADCAST_MESSAGE = "broadcastAction";

    }

    interface URL {
        String BASE_URL = "https://api.learn2crack.com/";

    }

    interface InternalHttpCode {
        int SUCCESS_CODE = 200;
        int UN_AUTH_CODE = 401;
        int INTERNAL_ERROR = 500;
        int FORCE_UPDATE = 426;
        int API_ON_FAILURE = 999;

    }

    //Make the code unique
    interface APiRequestCodes {
        long LOGIN_API = 1;
        long SIGN_IN_API = 2;
        long LIST_API = 3;
        int REQUEST_SINGLE_PERMISSION =99;
        int REQUEST_MULTIPLE_PERMISSION=89;

    }

    interface Permission {
        int SMS_READ_REQUEST_CODE = 10001;
    }

    interface Common {
        int SPLASH_TIMEOUT = 3000;
    }


    interface ApiHeaderKeys {

        String HEADER_CONTENT_TYPE = "Content-Type: application/json";

        String HEADER_AUTH_TOKEN = "Authorization";

        String HEADER_API_VERSION = "app-version: " + BuildConfig.VERSION_NAME;

        String HEADER_DEVICE_OS = "RO-MODEL: android";

        String HEADER_OS_INFO = "RO-OS-INFO: " + Build.MODEL;
    }

    interface SocketListenKey {
        String NEW_TAB = "newTab";

    }

    interface SocketEmitKey {
        String ORDER_DONE = "tabCartConfirm";

    }

}
