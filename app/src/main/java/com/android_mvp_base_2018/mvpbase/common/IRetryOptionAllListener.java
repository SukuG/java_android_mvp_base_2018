package com.android_mvp_base_2018.mvpbase.common;

/**
 * Created by Sukumar on 25-02-2018
 */

public interface IRetryOptionAllListener {
    void onClickRetryOption(boolean isRetryOption);
}
