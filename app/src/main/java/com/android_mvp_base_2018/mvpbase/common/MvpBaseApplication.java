package com.android_mvp_base_2018.mvpbase.common;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;

import com.android_mvp_base_2018.mvpbase.util.CodeSnippet;


/**
 * Created by sukumar on 04-02-2018
 */

public class MvpBaseApplication extends Application {

    private String TAG = "MvpBaseApplication";
    private static MvpBaseApplication mAppController;

    public static MvpBaseApplication getInstance() {
        if (mAppController == null)
            mAppController = new MvpBaseApplication();
        return mAppController;

    }


    @Override
    public void onCreate() {
        super.onCreate();
        mAppController = this;
        CodeSnippet.getInstance(this);
    }

    public ConnectivityManager getConnectivityManager() {
        return (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    }
}
