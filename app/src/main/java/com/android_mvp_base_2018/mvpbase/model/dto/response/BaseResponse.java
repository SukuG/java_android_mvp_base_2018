package com.android_mvp_base_2018.mvpbase.model.dto.response;

import com.bluelinelabs.logansquare.annotation.JsonObject;


/**
 * Created by sukumar on 04-02-2018
 */
@JsonObject(fieldDetectionPolicy = JsonObject.FieldDetectionPolicy.NONPRIVATE_FIELDS_AND_ACCESSORS)
public class BaseResponse {

    private int status;
    private String message;
    private String error;


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
