package com.android_mvp_base_2018.mvpbase.model.dto.request;

import com.bluelinelabs.logansquare.annotation.JsonObject;


/**
 * Created by sukumar on 04-02-2018
 */
@JsonObject(fieldDetectionPolicy = JsonObject.FieldDetectionPolicy.NONPRIVATE_FIELDS_AND_ACCESSORS)
public class BaseRequest {

    private int status;

    private String message;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
