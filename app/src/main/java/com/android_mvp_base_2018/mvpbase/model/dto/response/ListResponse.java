package com.android_mvp_base_2018.mvpbase.model.dto.response;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.util.List;

/**
 * Created by Sukumar on 25-02-2018
 */
@JsonObject(fieldDetectionPolicy = JsonObject.FieldDetectionPolicy.NONPRIVATE_FIELDS_AND_ACCESSORS)
public class ListResponse extends BaseResponse {

    @JsonField(name = "android")
    private List<ListData> androidOSVersionList;

    public List<ListData> getAndroidOSVersionList() {
        return androidOSVersionList;
    }

    public void setAndroidOSVersionList(List<ListData> androidOSVersionList) {
        this.androidOSVersionList = androidOSVersionList;
    }
}
