package com.android_mvp_base_2018.mvpbase.view.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.android_mvp_base_2018.library.CustomException;
import com.android_mvp_base_2018.mvpbase.common.IPermissionResponseListener;
import com.android_mvp_base_2018.mvpbase.constructs.iconstruct.IBasePresenter;
import com.android_mvp_base_2018.mvpbase.constructs.iconstruct.IBaseView;

import butterknife.ButterKnife;

import static android.view.View.inflate;

/**
 * Created by Sukumar on 06-02-2018
 */

public abstract class BaseDialogFragment<T extends IBasePresenter> extends android.support.v4.app.DialogFragment implements IBaseView {

    protected String TAG = getClass().getSimpleName();
    private T iBasePresenter;

    @NonNull
    abstract T bindViewPresenter(@Nullable Bundle savedInstanceState);

    protected abstract int getLayoutId();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getDialog().getWindow() != null) {
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        }
        return inflate(getContext(), getLayoutId(), null);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        iBasePresenter = bindViewPresenter(savedInstanceState);
        iBasePresenter.onCreatePresenter(getArguments());
    }

    @Override
    public void onStart() {
        super.onStart();
        if (iBasePresenter != null) iBasePresenter.onStartPresenter();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (iBasePresenter != null) iBasePresenter.onPausePresenter();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (iBasePresenter != null) iBasePresenter.onResumePresenter();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (iBasePresenter != null) iBasePresenter.onStopPresenter();
    }


    @Override
    public void showMessage(String message) {
        if (getActivity() != null)
            ((IBaseView) getActivity()).showMessage(message);
    }

    @Override
    public void showMessage(int resId) {
        if (getActivity() != null)
            ((IBaseView) getActivity()).showMessage(resId);
    }

    @Override
    public void showMessage(CustomException e) {
        if (getActivity() != null)
            ((IBaseView) getActivity()).showMessage(e);
    }

    @Override
    public void showProgressbar() {
        if (getActivity() != null)
            ((IBaseView) getActivity()).showProgressbar();
    }

    @Override
    public void dismissProgressbar() {
        if (getActivity() != null)
            ((IBaseView) getActivity()).dismissProgressbar();
    }

    @Override
    public void showSnackBar(String message) {
        if (getActivity() != null)
            ((IBaseView) getActivity()).showSnackBar(message);
    }

    @Override
    public void showNetworkErrorMessage() {
        if (getActivity() != null)
            ((IBaseView) getActivity()).showNetworkErrorMessage();
    }

    @Override
    public void showPermissionDenied() {
        if (getActivity() != null)
            ((IBaseView) getActivity()).showPermissionDenied();
    }

    @Override
    public void showSnackBar(@NonNull View view, String message) {
        if (getActivity() != null)
            ((IBaseView) getActivity()).showSnackBar(view, message);
    }

    @Override
    public void showUnAuthWindow() {
        if (getActivity() != null)
            ((IBaseView) getActivity()).showUnAuthWindow();
    }

    @Override
    public void showForceUpdateDialog() {
        if (getActivity() != null)
            ((IBaseView) getActivity()).showForceUpdateDialog();
    }

    @Override
    public void showRetryOption() {
        if (getActivity() != null)
            ((IBaseView) getActivity()).showRetryOption();
    }

    @Override
    public boolean isNetworkEnabled() {
        return getActivity() != null && ((IBaseView) getActivity()).isNetworkEnabled();
    }


    //need to verify.Either working or not inside DialogFragment
    @Override
    public void requestSinglePermission(String permission, IPermissionResponseListener permissionResponseListener) {
        if (getActivity() != null)
            ((IBaseView) getActivity()).requestSinglePermission(permission, permissionResponseListener);
    }
    //need to verify.Either working or not inside DialogFragment
    @Override
    public void requestMultiplePermission(String[] permission, String[] mustPermissionValues, IPermissionResponseListener permissionResponseListener) {
        if (getActivity() != null)
            ((IBaseView) getActivity()).requestMultiplePermission(permission, mustPermissionValues, permissionResponseListener);
    }
}
