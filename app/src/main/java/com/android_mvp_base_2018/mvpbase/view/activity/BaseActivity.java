package com.android_mvp_base_2018.mvpbase.view.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Toast;

import com.android_mvp_base_2018.library.CustomException;
import com.android_mvp_base_2018.library.ExceptionTracker;
import com.android_mvp_base_2018.mvpbase.common.IPermissionResponseListener;
import com.android_mvp_base_2018.mvpbase.constructs.iconstruct.IBasePresenter;
import com.android_mvp_base_2018.mvpbase.constructs.iconstruct.IBaseView;
import com.android_mvp_base_2018.mvpbase.util.CodeSnippet;
import com.android_mvp_base_2018.mvpbase.view.widget.CustomProgressbar;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

import static com.android_mvp_base_2018.mvpbase.common.Constants.APiRequestCodes.REQUEST_MULTIPLE_PERMISSION;
import static com.android_mvp_base_2018.mvpbase.common.Constants.APiRequestCodes.REQUEST_SINGLE_PERMISSION;


/**
 * Created by sukumar on 04-02-2018
 */
public abstract class BaseActivity<T extends IBasePresenter> extends AppCompatActivity implements IBaseView {

    protected String TAG = getClass().getSimpleName();
    protected View mView;
    ProgressDialog pDialog;
    protected T iBasePresenter;
    private CustomProgressbar mCustomProgressbar;

    private IPermissionResponseListener iPermissionResponseListener;
    private String[] mPermissionMustValues = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        injectViews();
        iBasePresenter = bindViewPresenter(savedInstanceState);
        iBasePresenter.onCreatePresenter(getIntent().getExtras());
    }

    @NonNull
    abstract T bindViewPresenter(@Nullable Bundle savedInstanceState);

    @Override
    public View onCreateView(String name, Context context, AttributeSet attrs) {
        mView = getWindow().getDecorView().findViewById(android.R.id.content);
        return super.onCreateView(name, context, attrs);
    }

    private void injectViews() {
        ButterKnife.bind(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (iBasePresenter != null) iBasePresenter.onStartPresenter();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (iBasePresenter != null) iBasePresenter.onStopPresenter();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (iBasePresenter != null) iBasePresenter.onPausePresenter();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (iBasePresenter != null) iBasePresenter.onResumePresenter();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (iBasePresenter != null) iBasePresenter.onDestroyPresenter();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (iBasePresenter != null)
            iBasePresenter.onActivityResultPresenter(requestCode, resultCode, data);
    }

    private CustomProgressbar getProgressBar() {
        if (mCustomProgressbar == null) {
            mCustomProgressbar = new CustomProgressbar(this);
        }
        return mCustomProgressbar;
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showMessage(int resId) {
        Toast.makeText(this, getString(resId), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showMessage(CustomException e) {
        Toast.makeText(this, e.getException(), Toast.LENGTH_SHORT).show();
    }


    @Override
    public void showProgressbar() {
        if (getProgressBar() != null)
            getProgressBar().show();
    }

    @Override
    public void dismissProgressbar() {
        runOnUiThread(() -> {
            try {
                getProgressBar().dismissProgress();
            } catch (Exception e) {
                ExceptionTracker.track(e);
            }
        });
    }

    public void showLoadingDialog(Context context) {

        pDialog = new ProgressDialog(context);
        pDialog.setMessage(Html.escapeHtml("<b>Please wait auto read otp processing...</b>"));
        pDialog.setCancelable(false);
        pDialog.show();
    }

    public void closeLoadingDialog() {

        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }

    }


    @Override
    public FragmentActivity getActivity() {
        return this;
    }

    @Override
    public void showSnackBar(String message) {
        if (mView != null) {
            Snackbar snackbar = Snackbar.make(mView, message, Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            snackbar.show();
        }
    }

    @Override
    public void showSnackBar(@NonNull View view, String message) {
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
        snackbar.setActionTextColor(Color.RED);
        snackbar.show();
    }

    @Override
    public void showNetworkErrorMessage() {
        if (mView != null) {
            Snackbar snackbar = Snackbar.make(mView, "Internet not found!", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            snackbar.setAction("Settings", view -> CodeSnippet.mCodeSnippet.showNetworkSettings());
            snackbar.show();
        }
    }

    @Override
    public void showPermissionDenied() {
        if (mView != null) {
            Snackbar snackbar = Snackbar.make(mView, "Permission Denied.", Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            snackbar.setAction("Go to Settings", view -> CodeSnippet.mCodeSnippet.showPermissionSettings());
            snackbar.show();
        }
    }

    /**
     * Returns the layout id.
     * <p>
     * TAG @return
     */

    protected abstract int getLayoutId();


    @Override
    public boolean isNetworkEnabled() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo info = connectivityManager.getActiveNetworkInfo();
            if (info != null) {
                if (info.getState() == NetworkInfo.State.CONNECTED) {
                    return true;
                }
            }
        }
        return false;
    }


    @Override
    public void showUnAuthWindow() {
        //show logout dialog
    }

    @Override
    public void showForceUpdateDialog() {
        //show force update dialog
    }

    @Override
    public void showRetryOption() {
        //don't implement here. Implement respect activity/fragment
    }


    @Override
    public void requestSinglePermission(String permission, IPermissionResponseListener permissionResponseListener) {
        this.iPermissionResponseListener = permissionResponseListener;

        if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {

            //note: if you want show explanation then implement here
            /*if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {

            }*/

            // else No explanation needed; request the permission
            ActivityCompat.requestPermissions(this, new String[]{permission}, REQUEST_SINGLE_PERMISSION);
        } else {
            //permission already granted
            iPermissionResponseListener.onClickSinglePermissionStatus(true);
        }
    }

    @Override
    public void requestMultiplePermission(String[] permission, String[] mustPermissionValues, IPermissionResponseListener permissionResponseListener) {
        this.iPermissionResponseListener = permissionResponseListener;
        this.mPermissionMustValues = mustPermissionValues;

        if (permission == null)
            return;

        List<String> stringPermissions = new ArrayList<>();
        for (int i = 0; i < permission.length; i++) {
            if (ContextCompat.checkSelfPermission(this, permission[i]) != PackageManager.PERMISSION_GRANTED) {
                stringPermissions.add(permission[i]);
            }
        }
        //now check all permission granter or not
        if (stringPermissions.size() == 0)// zero mean, all permission already granted.
            iPermissionResponseListener.onClickMultiplePermissionStatus(true);
        else {
            //note: if you want show explanation then implement here
            /*if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {

            }*/
            // else No explanation needed; request the permission
            ActivityCompat.requestPermissions(this, stringPermissions.toArray(new String[stringPermissions.size()]), REQUEST_MULTIPLE_PERMISSION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_SINGLE_PERMISSION:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted;
                    iPermissionResponseListener.onClickSinglePermissionStatus(true);
                } else {
                    // permission denied,
                    iPermissionResponseListener.onClickSinglePermissionStatus(false);
                }

                break;

            case REQUEST_MULTIPLE_PERMISSION:

               /* boolean allGranted = false;
                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        allGranted = true;
                    } else {
                        allGranted = false;
                        break;
                    }
                }
                //check whether all granted or not
                if (allGranted) {
                    //all permission  granted
                    iPermissionResponseListener.onClickMultiplePermissionStatus(true);
                } else {
                    //permission not granted
                    iPermissionResponseListener.onClickMultiplePermissionStatus(false);
                }*/

                boolean mustGrantedByUser = false;
                if (mPermissionMustValues == null)
                    return;

                for (int i = 0; i < mPermissionMustValues.length; i++) {
                    if (ContextCompat.checkSelfPermission(this, mPermissionMustValues[i]) == PackageManager.PERMISSION_GRANTED) {
                        mustGrantedByUser = true;
                    } else {
                        mustGrantedByUser = false;
                        break;
                    }
                }
                //check whether all granted or not
                if (mustGrantedByUser) {
                    //all permission  granted
                    iPermissionResponseListener.onClickMultiplePermissionStatus(true);
                } else {
                    //permission not granted
                    iPermissionResponseListener.onClickMultiplePermissionStatus(false);
                }
                break;
        }
    }
}