package com.android_mvp_base_2018.mvpbase.view.activity;


import android.Manifest;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.android_mvp_base_2018.R;
import com.android_mvp_base_2018.mvpbase.adapters.ListAdapter;
import com.android_mvp_base_2018.mvpbase.common.IPermissionResponseListener;
import com.android_mvp_base_2018.mvpbase.common.IRetryOptionAllListener;
import com.android_mvp_base_2018.mvpbase.constructs.HomePresenter;
import com.android_mvp_base_2018.mvpbase.constructs.iconstruct.IHomeConstruct;
import com.android_mvp_base_2018.mvpbase.util.CodeSnippet;

import butterknife.BindView;


public class HomeActivity extends BaseActivity<IHomeConstruct.IHomePresenter> implements IHomeConstruct.IHomeView, SwipeRefreshLayout.OnRefreshListener {


    @BindView(R.id.list)
    RecyclerView vRecyclerView;

    @BindView(R.id.listRefresh)
    SwipeRefreshLayout vSwipeRefreshLayout;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }


    @NonNull
    @Override
    IHomeConstruct.IHomePresenter bindViewPresenter(@Nullable Bundle savedInstanceState) {
        setListerners();
        return new HomePresenter(this);
    }

    private void setListerners() {
        vSwipeRefreshLayout.setOnRefreshListener(this);
        requestPermission();

    }

    private void requestPermission() {
        requestMultiplePermission(
                new String[]
                        {Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.READ_CONTACTS,
                                Manifest.permission.READ_PHONE_STATE,
                                Manifest.permission.READ_SMS},
                new String[]
                        {Manifest.permission.READ_CONTACTS},
                iPermissionResponseListener);
    }


    @Override
    public void setAdapter(ListAdapter mListAdapter) {
        vRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        vRecyclerView.setAdapter(mListAdapter);


    }

    //define below two method if you want retry option
    @Override
    public void showRetryOption() {
        super.showRetryOption();
        //implement Retry option here not BaseActivity
        //Note: 1. If retry option UI same for entire app then call below method.
        CodeSnippet.mCodeSnippet.showRetryOptionEntireApp(getActivity(), iRetryOptionAllListener);
        //Note: 2. if retry option UI is different each and every screen then create separately.
    }

    private IRetryOptionAllListener iRetryOptionAllListener = isRetryOption -> {
        if (isRetryOption) {
            iBasePresenter.callLoginApi();
        } else
            showSnackBar("Retry Option Cancel");
    };

    @Override
    public void onRefresh() {
        vSwipeRefreshLayout.setRefreshing(false);
        iBasePresenter.callLoginApi();
    }

    private IPermissionResponseListener iPermissionResponseListener = new IPermissionResponseListener() {
        @Override
        public void onClickSinglePermissionStatus(boolean isGranted) {
            if (isGranted) {
                showSnackBar("Single permission granted");
            } else {
                showSnackBar("Single permission NOT granted");
            }
        }

        @Override
        public void onClickMultiplePermissionStatus(boolean isGranted) {
            if (isGranted) {
                showSnackBar("Multiple permission granted");
            } else {
                showPermissionDenied();
            }
        }
    };

}
